#include <stdio.h>
#include <stdlib.h>


struct node 
{
	int key;
	struct node *next;
};

typedef struct node Tnode;




		// This function creates a node
Tnode *create_node(int key)
{
	Tnode *new_n = malloc(sizeof(Tnode));
	new_n->key = key;
	new_n->next = NULL;

	return new_n;
}

		// This is the function that links a circulary list
void link_circulary_list(Tnode **pCList, Tnode *node_to_insert)
{
	Tnode *tmp = *pCList;

	if(*pCList != NULL)
	{
		while(tmp->next != *pCList)
			tmp=tmp->next;

		if(tmp->next == *pCList)
		{
			node_to_insert->next = *pCList;
			tmp->next = node_to_insert;
		}
	}
	else
	{
			// Here we link single node to itself
		*pCList = node_to_insert;
		(*pCList)->next = *pCList;
	}
}


void print_list(Tnode *pCList)
{	
	Tnode *tmp = pCList;
	do
	{
		printf("%d - ", tmp->key);
		tmp = tmp->next;
	}
	while(tmp->next != pCList);

	printf("\n");
}


int main()
{
	Tnode *pCList=NULL;
	Tnode *tmp;

	tmp = create_node(12);
	link_circulary_list(&pCList, tmp);
	print_list(pCList);

	return 0;
}

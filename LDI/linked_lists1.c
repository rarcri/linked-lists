#include <stdlib.h>
#include <stdio.h> 

										// Struct declaration //
struct nod
{
	int value;

	struct nod *prev;
	struct nod *next;

} Node1;

		// I defined a new type of nod
typedef struct nod node_t;
				
		// These are the functions that return pointer to node_t 
		// FUNCTIONS (lines 57-269) 
node_t *create_node(int value);
node_t *insert_before_head(node_t **head, node_t *node_to_insert);
node_t *insert_to_end(node_t **head, node_t *node_to_insert);
node_t *find(node_t *head, int value);

		// These functions return nothing
		// FUNCTIONS 
void insert(node_t **head, int key, node_t *node_to_insert, int choice);
void insert_to_begining(node_t **head, node_t *node_to_insert);
void delete_first(node_t **head);
void delete_last(node_t *head);
void delete(node_t **head, int value_to_find);
void delete_all(node_t **head);

		// These functions use the FUNCTIONS 
		// AUXILIARY FUNCTIONS (lines 273-379) 
void cin_node(node_t **head); 
void cin_new_node(node_t **head);
void cin_new_node_to_beginning(node_t **head);
void cin_new_node_to_end(node_t **head);
void cin_new_node_before(node_t **head);
void cin_new_node_after(node_t **head);
void cin_delete(node_t **head);

		// Functia de afisare a unei liste
void afisare_lista(node_t *head);
void afisare_inversa(node_t *head);

		// MENU FUNCTIONS
int menu(node_t *head);
void main_menu(node_t **head);


int main()
{
	node_t *head=NULL;
	main_menu(&head); // line 426
	return 0;
}

											// FUNCTIONS //

node_t *create_node(int value)
{
	node_t *new1 = malloc(sizeof(node_t));
	new1->value = value;
	new1->prev = NULL;
	new1->next = NULL;

	return new1;
}


node_t *insert_before_head(node_t **head, node_t *node_to_insert)
{
	node_to_insert->next=*head;
	*head=node_to_insert;

	return node_to_insert;
}



node_t *insert_to_end(node_t **head, node_t *node_to_insert)
{
	node_t *temporary = *head;

	if(temporary != NULL)
	{
		while(temporary->next != NULL)
			temporary=temporary->next;

		node_to_insert->prev = temporary;
		temporary->next = node_to_insert;
		temporary=node_to_insert;
    } 
	else 
		*head = node_to_insert;
	
	return node_to_insert;
}


node_t *find(node_t *t_head, int value)
{	
	node_t *null=NULL;

	if(t_head != NULL)
	{
		while(t_head->value != value)
		{
			if(t_head->next != NULL)
				t_head = t_head->next;
			else
			{	
				printf("	Cheia nu s-a gasit");
				return null;
			}
		}
		
		return t_head;
	}
	else
		printf("	Lista este goala.");
}


void insert(node_t **head, int key, node_t *node_to_insert, int choice)
{
	node_t *temporary = *head;
	
	if(temporary != NULL)
	{   
				// If first has the key 
		if(temporary->value == key)
		{			
					// Insert to beginning
			if(choice == 0) 
			{
				node_to_insert->next = temporary;
				(*head)->prev = node_to_insert; 
				*head = node_to_insert;
			}
					// Insert after head
			else if(choice == 1)
			{
				node_to_insert->prev = *head;
				node_to_insert->next = (*head)->next;
				(*head)->next->prev = node_to_insert;
				(*head)->next = node_to_insert;
			}
			return;
		}

		while(temporary->value != key)
		{
			if(temporary->next != NULL)
				temporary = temporary->next;
			else
			{
				printf("	Cheia nu s-a gasit!");
				return;
			}
		}

				// End of the list			
		if(temporary->next == NULL)
		{
			if(choice == 1)
			{
				node_to_insert->prev = temporary;
				temporary->next=node_to_insert;
			}
			else if(choice == 0)
			{	
				node_to_insert->prev = temporary->prev;
				node_to_insert->next = temporary;
				temporary->prev->next = node_to_insert;
				temporary->prev = node_to_insert;
			}
		}
				// Middle of the list
		else
		{
			if(choice == 1)
			{	
				node_to_insert->prev = temporary;
				node_to_insert->next = temporary->next;
				temporary->next->prev = node_to_insert;
				temporary->next=node_to_insert;
			}	
			else if(choice == 0)
			{
				node_to_insert->prev = temporary->prev;
				node_to_insert->next = temporary;
				temporary->prev->next = node_to_insert;
				temporary->prev = node_to_insert;
			}
		}
				
	} 
	else 
		printf("	Lista este goala");
}


void delete_first(node_t **head)
{
	node_t *tmp = *head;
	if(*head != NULL)
	{
				// If list has more than 1 element
		if((*head)->next != NULL)
		{
			*head = (*head)->next;
			(*head)->next->prev = NULL;
		}
			// If list has only 1 element			
		free(tmp);
	}
	else
		printf("Lista este vida.");
}

		// Functia de stergere a ultimului nod
void delete_last(node_t *head)
{	
	if(head != NULL)
	{
		if(head->next == NULL)
			free(head);
		else
		{
			while(head->next != NULL)
				head = head->next;

			head->prev->next = NULL;
			free(head);
		}
	}
	else
		printf("\n    Lista este goala.");
}


void delete(node_t **head, int value_to_find)
{
	node_t *tmp;
	tmp = *head;

	if(tmp != NULL)
	{
		if(tmp->next == NULL)
			free(tmp);

		if(tmp->value == value_to_find)
		{
			*head = (*head)->next;
			(*head)->next->prev = NULL;
			free(tmp);
		}

		while(tmp->value != value_to_find)
		{
			if(tmp->next != NULL)
				tmp = tmp->next;
			else
			{
				printf("	Cheia nu a fost gasita\n");
				return;
			}
		}
	
		if(tmp->next != NULL)
		{
			tmp->prev->next = tmp->next;
			tmp->next->prev = tmp->prev;
			free(tmp);
		}
		else
		{
			tmp->prev->next = NULL;
			free(tmp);
		}
		printf("\n    Nodul cu cheia %d a fost sters\n", value_to_find);
	}
	else 
		printf("\nLista este goala.");
}


void delete_all(node_t **head)
{
	while((*head)->next != NULL)
		delete_first(&*head);
			// If only one element in list 
	delete_first(&*head);
}

										// AUXILIARY FUNCTIONS //

void cin_node(node_t **head)
{
	node_t *tmp;
	int value;
	int n;
	printf("\n    Dati numarul de noduri: "); scanf("%d", &n);
	printf("\n");

	for(int i=0; i<n; i++)
	{
		printf("    Dati cheia nodului %d: ", i+1);
		scanf("%d", &value);
		tmp = create_node(value);
		insert_to_end(head, tmp);
	}
}


void cin_new_node(node_t **head)
{
	node_t *tmp = NULL;
	int n_value, value, choice;

	printf("    Dati cheia noului nod: ");
	scanf("%d", &n_value);

	printf("    Dati cheia față de care vreți să introduceți nodul: ");
	scanf("%d", &value);

	printf("    Inserare inainte/dupa(0/1) de nodul cu cheia %d: ", value);
	scanf("%d", &choice);
	
	tmp = create_node(n_value);
	insert(&*head, value, tmp, choice);
}


void cin_new_node_to_beginning(node_t **head)
{
	node_t *tmp; 
	int n_value;
	
	printf("    Dati valoarea noului nod: ");
	scanf("%d", &n_value);

	tmp = create_node(n_value);
	insert_before_head(&*head, tmp);
}


void cin_new_node_to_end(node_t **head)
{
	node_t *tmp;
	int n_value;

	printf("    Dati valoare noului nod: ");
	scanf("%d", &n_value);

	tmp = create_node(n_value);

	insert_to_end(&*head, tmp);
}


void cin_new_node_before(node_t **head)
{
	node_t *tmp = NULL;
	int n_value, value;

	printf("    Dati valoarea noului nod: ");
	scanf("%d", &n_value);

	printf("    Dati valoarea inainte de care vreți să introduceți nodul: ");
	scanf("%d", &value);

	tmp = create_node(n_value);
	insert(&*head, value, tmp, 0);
	
}


void cin_new_node_after(node_t **head)
{
	node_t *tmp = NULL;
	int n_value, value;

	printf("    Dati valoarea noului nod: ");
	scanf("%d", &n_value);

	printf("    Dati valoarea dupa care vreți să introduceți nodul: ");
	scanf("%d", &value);

	tmp = create_node(n_value);
	insert(&*head, value, tmp, 1);
	
}


void cin_delete(node_t **head)
{
	int key;

	printf("\n    Care este cheia nodului pe care doriti sa il stergeti: ");
	scanf("%d", &key);

	delete(&*head, key);	
}

										// FUNCTIA DE AFISARE A LISTEI // 

void afisare_lista(node_t *head)
{
	node_t *temporary = head;

	if(temporary == NULL)
		printf("          Lista este goala");

	printf("          ");
	while(temporary != NULL)
	{
		printf("%d - ", temporary->value);
		temporary = temporary->next;
	}

	printf("\n");
}

void afisare_inversa(node_t *head)
{
	if(head == NULL)
		printf("       Lista este goala");

	while(head->next != NULL)
		head = head->next;

	while(head != NULL)
	{
		printf("%d - ", head->value);
		head = head->prev;
	}

	printf("\n");
}

int menu(node_t *head)
{
	int choice;

	system("cls");
	printf("\n	          _____________________MENIU__________________\n\
	          | Citeste o lista de la tastatura     - 1  |\n\
                  | Insereaza un nod la inceput         - 2  |\n\
	          | Insereaza un nod la final           - 3  |\n\
	          | Insereaza un nod inaintea unei chei - 4  |\n\
	          | Insereaza un nod dupa o cheie       - 5  |\n\
	          | Sterge primul nod                   - 6  |\n\
	          | Sterge ultimul nod                  - 7  |\n\
	          | Sterge un nod dupa cheie            - 8  |\n\
	          | Sterge toata lista                  - 9  |\n\
	          | Terminare                           - 10 |\n\
	          |__________________________________________|\n\n");
	printf("    Cum doriti sa afisati lista ?(normal/invers)\n    (0/1): ");
	scanf("%d", &choice);

	printf("    Lista actuala: ");
	if(choice == 1)
		afisare_inversa(head);
	else
		afisare_lista(head);

	printf("\n    Alegeti o optiune: ");
	scanf("%d", &choice);

	return choice;
}

void main_menu(node_t **head)
{
	int choice=1;

	while(choice < 10)
	{
		choice = menu(*head);

		switch(choice)
		{
			case 1:
				cin_node(&*head);
				break;
			case 2:
				cin_new_node_to_beginning(&*head);
				break;
			case 3: 
				cin_new_node_to_end(&*head);
				break;
			case 4:
				cin_new_node_before(&*head);
				break;
			case 5:
				cin_new_node_after(&*head);
				break;
			case 6:
				delete_first(&*head);	
				break;
			case 7:
				delete_last(*head);
				break;
			case 8:
				cin_delete(&*head);
				break;
			case 9:
				delete_all(&*head);
				break;
			
			default:
		    printf("    Programul s-a terminat\n\n");
		}
	}
}


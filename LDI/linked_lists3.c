#include <stdlib.h>
#include <stdio.h> 
#include <string.h>

										// Struct declaration //
struct nod
{
	int id;

	struct nod *prev;
	struct nod *next;

} Node1;

		// I defined a new type of nod
typedef struct nod node_t;
				
		// These are the functions that return pointer to node_t 
		// FUNCTIONS (lines 57-269) 
node_t *create_node(int id);
node_t *insert_before_head(node_t **head, node_t *node_to_insert);
node_t *insert_to_end(node_t **head, node_t *node_to_insert);
node_t *find(node_t *head, int id);
node_t *load_node(node_t **head);

		// These functions return nothing
		// FUNCTIONS 
void insert(node_t **head, int key, node_t *node_to_insert, int choice);
void load_node_from_file(node_t **head);
void insert_to_begining(node_t **head, node_t *node_to_insert);
void delete_first(node_t **head);
void delete_last(node_t *head);
void delete(node_t **head, int id_to_find);
void delete_all(node_t **head);

		// These functions use the FUNCTIONS 
		// AUXILIARY FUNCTIONS (lines 273-379) 
void cin_nodes(node_t **head); 
void cin_new_node(node_t **head);
void cin_new_node_to_beginning(node_t **head);
void cin_new_node_to_end(node_t **head);
void cin_new_node_before(node_t **head);
void cin_new_node_after(node_t **head);
void cin_delete(node_t **head);

		// Functia de afisare a unei liste
void afisare_lista(node_t *head);
void afisare_inversa(node_t *head);

		// MENU FUNCTIONS
int menu(node_t *head);
void main_menu(node_t **head);


int main()
{
	node_t *head=NULL;
	load_node_from_file(&head);
	main_menu(&head); // line 426
	return 0;
}

											// FUNCTIONS //

node_t *create_node(int id)
{
	node_t *new1 = malloc(sizeof(node_t));

	new1->id = id;

	new1->prev = NULL;
	new1->next = NULL;

	return new1;
}


node_t *load_node(node_t **head)
{
	node_t *tmp;
	int id;

	printf("    Dati id-ul persoanei: ");
	scanf("%d", &id);

	tmp = create_node(id);

	return tmp;
}

void load_node_from_file(node_t **head)
{
	node_t *tmp;

	FILE *fptr;
	fptr = fopen("numbers.txt", "r");
	
	int id;

	for(int i=0; i<16; i++)
	{
		fscanf(fptr, "%d", &id);

		tmp = create_node(id);
		insert_to_end(&*head, tmp);
	}
}


node_t *insert_before_head(node_t **head, node_t *node_to_insert)
{
	node_to_insert->next=*head;
	*head=node_to_insert;

	return node_to_insert;
}


node_t *insert_to_end(node_t **head, node_t *node_to_insert)
{
	node_t *temporary = *head;

	if(temporary != NULL)
	{
		while(temporary->next != NULL)
			temporary=temporary->next;

		node_to_insert->prev = temporary;
		temporary->next = node_to_insert;
		temporary=node_to_insert;
    } 
	else 
		*head = node_to_insert;
	
	return node_to_insert;
}


node_t *find(node_t *t_head, int id)
{	
	node_t *null=NULL;

	if(t_head != NULL)
	{
		while(t_head->id != id)
		{
			if(t_head->next != NULL)
				t_head = t_head->next;
			else
			{	
				printf("	Cheia nu s-a gasit");
				return null;
			}
		}
		
		return t_head;
	}
	else
		printf("	Lista este goala.");
}


void insert(node_t **head, int key, node_t *node_to_insert, int choice)
{
	node_t *temporary = *head;
	
	if(temporary != NULL)
	{   
				// If first has the key 
		if(temporary->id == key)
		{			
					// Insert to beginning
			if(choice == 0) 
			{
				node_to_insert->next = temporary;
				(*head)->prev = node_to_insert; 
				*head = node_to_insert;
			}
					// Insert after head
			else if(choice == 1)
			{
				node_to_insert->prev = *head;
				node_to_insert->next = (*head)->next;
				(*head)->next->prev = node_to_insert;
				(*head)->next = node_to_insert;
			}
			return;
		}

		while(temporary->id != key)
		{
			if(temporary->next != NULL)
				temporary = temporary->next;
			else
			{
				printf("	Id-ul nu s-a gasit!");
				return;
			}
		}

				// End of the list			
		if(temporary->next == NULL)
		{
			if(choice == 1)
			{
				node_to_insert->prev = temporary;
				temporary->next=node_to_insert;
			}
			else if(choice == 0)
			{	
				node_to_insert->prev = temporary->prev;
				node_to_insert->next = temporary;
				temporary->prev->next = node_to_insert;
				temporary->prev = node_to_insert;
			}
		}
				// Middle of the list
		else
		{
			if(choice == 1)
			{	
				node_to_insert->prev = temporary;
				node_to_insert->next = temporary->next;
				temporary->next->prev = node_to_insert;
				temporary->next=node_to_insert;
			}	
			else if(choice == 0)
			{
				node_to_insert->prev = temporary->prev;
				node_to_insert->next = temporary;
				temporary->prev->next = node_to_insert;
				temporary->prev = node_to_insert;
			}
		}
				
	} 
	else 
		printf("	Lista este goala");
}


void delete_first(node_t **head)
{
	node_t *tmp = *head;
	if(*head != NULL)
	{
				// If list has more than 1 element
		if((*head)->next != NULL)
		{
			*head = (*head)->next;
			(*head)->next->prev = NULL;
		}
			// If list has only 1 element			
		free(tmp);
	}
	else
		printf("Lista este vida.");
}

		// Functia de stergere a ultimului nod
void delete_last(node_t *head)
{	
	if(head != NULL)
	{
		if(head->next == NULL)
			free(head);
		else
		{
			while(head->next != NULL)
				head = head->next;

			head->prev->next = NULL;
			free(head);
		}
	}
	else
		printf("\n    Lista este goala.");
}


void delete(node_t **head, int id_to_find)
{
	node_t *tmp;
	tmp = *head;

	if(tmp != NULL)
	{
		if(tmp->next == NULL)
			free(tmp);

		if(tmp->id == id_to_find)
		{
			*head = (*head)->next;
			(*head)->next->prev = NULL;
			free(tmp);
		}

		while(tmp->id != id_to_find)
		{
			if(tmp->next != NULL)
				tmp = tmp->next;
			else
			{
				printf("	Id-ul nu a fost gasit\n");
				return;
			}
		}
	
		if(tmp->next != NULL)
		{
			tmp->prev->next = tmp->next;
			tmp->next->prev = tmp->prev;
			free(tmp);
		}
		else
		{
			tmp->prev->next = NULL;
			free(tmp);
		}
		printf("\n    Persoana cu id-ul %d a fost stearsa\n", id_to_find);
	}
	else 
		printf("\nLista este goala.");
}


void delete_all(node_t **head)
{
	while((*head)->next != NULL)
		delete_first(&*head);
			// If only one element in list 
	delete_first(&*head);
}

										// AUXILIARY FUNCTIONS //

void cin_nodes(node_t **head)
{
	node_t *tmp;
	int id;
	char name[10];
	int birth_year;
	int n;

	printf("\n    Dati numarul de persoane: "); scanf("%d", &n);
	printf("\n");

	for(int i=0; i<n; i++)
	{
		tmp = load_node(&*head);
		printf("\n");
		insert_to_end(head, tmp);
	}
}


void cin_new_node(node_t **head)
{
	node_t *tmp = NULL;
	int id, choice;

	printf("    Dati id-ul față de care vreți să introduceți persoana: ");
	scanf("%d", &id);

	printf("    Inserare inainte/dupa(0/1) de persoana cu id-ul %d: ", id);
	scanf("%d", &choice);
	
	tmp = load_node(&*head);
	insert(&*head, id, tmp, choice);
}


void cin_new_node_to_beginning(node_t **head)
{
	node_t *tmp; 
	
	tmp = load_node(&*head);
	insert_before_head(&*head, tmp);
}


void cin_new_node_to_end(node_t **head)
{
	node_t *tmp;

	tmp = load_node(&*head); 
	insert_to_end(&*head, tmp);
}


void cin_new_node_before(node_t **head)
{
	node_t *tmp = NULL;
	int id;

	printf("    Dati id-ul inainte de care vreți să introduceți persoana: ");
	scanf("%d", &id);

	tmp = load_node(&*head);
	insert(&*head, id, tmp, 0);
	
}


void cin_new_node_after(node_t **head)
{
	node_t *tmp = NULL;
	int id;

	printf("    Dati id-ul dupa care vreți să introduceți persoana: ");
	scanf("%d", &id);

	tmp = load_node(&*head);
	insert(&*head, id, tmp, 1);
	
}


void cin_delete(node_t **head)
{
	int key;

	printf("\n    Care este id-ul persoanei pe care doriti sa il stergeti: ");
	scanf("%d", &key);

	delete(&*head, key);	
}

										// FUNCTIA DE AFISARE A LISTEI // 

void afisare_lista(node_t *head)
{
	node_t *temporary = head;

	if(temporary == NULL)
		printf("          Lista este goala");

	while(temporary != NULL)
	{
		printf("%d - ", temporary->id);
		temporary = temporary->next;
	}

	printf("\n");
}

void afisare_inversa(node_t *head)
{
	if(head == NULL)
		printf("       Lista este goala");

	while(head->next != NULL)
		head = head->next;

	while(head != NULL)
	{
		printf("%d - ", head->id);
		head = head->prev;
	}

	printf("\n");
}

int menu(node_t *head)
{
	int choice;

	system("clear");
	printf("\n	          _____________________MENIU____________________\n\
	          | Citeste o noua lista de la tastatura  - 1  |\n\
                  | Insereaza o persoana la inceput       - 2  |\n\
	          | Insereaza o persoana la final         - 3  |\n\
	          | Insereaza o persoana inainte de un id - 4  |\n\
	          | Insereaza o persoana dupa un id       - 5  |\n\
	          | Sterge prima persoana                 - 6  |\n\
	          | Sterge ultima persoana                - 7  |\n\
	          | Sterge o persoana dupa id             - 8  |\n\
	          | Sterge toata lista                    - 9  |\n\
	          | Terminare                             - 10 |\n\
	          |____________________________________________|\n\n");

	printf("    Lista actuala: ");
	afisare_lista(head);

	printf("\n    Alegeti o optiune: ");
	scanf("%d", &choice);

	return choice;
}

void main_menu(node_t **head)
{
	int choice=1;

	while(choice < 10)
	{
		choice = menu(*head);

		switch(choice)
		{
			case 1:
				cin_nodes(&*head);
				break;
			case 2:
				cin_new_node_to_beginning(&*head);
				break;
			case 3: 
				cin_new_node_to_end(&*head);
				break;
			case 4:
				cin_new_node_before(&*head);
				break;
			case 5:
				cin_new_node_after(&*head);
				break;
			case 6:
				delete_first(&*head);	
				break;
			case 7:
				delete_last(*head);
				break;
			case 8:
				cin_delete(&*head);
				break;
			case 9:
				delete_all(&*head);
				break;
			
			default:
		    printf("    Programul s-a terminat\n\n");
		}
	}
}


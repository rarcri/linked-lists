#include <stdlib.h>
#include <stdio.h> 
#include <string.h>

										// Struct declaration //
struct nod
{
	int id;

	struct nod *prev;
	struct nod *next;

} Node1;

typedef struct nod node_t;
				
node_t *create_node(int id);
node_t *insert_to_end(node_t **head, node_t *node_to_insert);
node_t *load_node(node_t **head);

void load_node_from_file(node_t **head);

		// These functions use the FUNCTIONS 
		// AUXILIARY FUNCTIONS (lines 273-379) 
void cin_nodes(node_t **head); 
void cin_new_node(node_t **head);
void cin_new_node_to_end(node_t **head);
void create_head_even(node_t *head, node_t **head_even);
void create_head_between(node_t *head,node_t **head_between);

		// Functia de afisare a unei liste
void afisare_lista(node_t *head);


int main()
{
	node_t *head=NULL;
	node_t *head_even=NULL;
	node_t *head_between=NULL;
			
			// Creare lista din fisierul numbers.txt
	load_node_from_file(&head);
	printf("\n    Lista principala este:       ");
			// Afisare lista head
	afisare_lista(head);
		
			// Crearea listei head_even din elementele pare din lista head 
	create_head_even(head, &head_even);

			// Afisarea listei head_even
	printf("\n    Lista cu elementele pare din prima este:    ");
	afisare_lista(head_even);


			// Crearea listei head_between din elementele din lista head
			// care sunt in intervalul introdus de la tastatura
	create_head_between(head, &head_between);

			// Afisarea listei head_between
	printf("\n    Lista cu elementele intre valorile introduse:     ");
	afisare_lista(head_between);

	return 0;
}

											// FUNCTIONS //
node_t *create_node(int id)
{
	node_t *new1 = malloc(sizeof(node_t));

	new1->id = id;

	new1->prev = NULL;
	new1->next = NULL;

	return new1;
}


node_t *load_node(node_t **head)
{
	node_t *tmp;
	int id;

	printf("    Dati id-ul nodului: ");
	scanf("%d", &id);

	tmp = create_node(id);

	return tmp;
}

void load_node_from_file(node_t **head)
{
	node_t *tmp;

	FILE *fptr;
	fptr = fopen("numbers.txt", "r");
	
	int id;

	for(int i=0; i<16; i++)
	{
		fscanf(fptr, "%d", &id);

		tmp = create_node(id);
		insert_to_end(&*head, tmp);
	}
}


node_t *insert_to_end(node_t **head, node_t *node_to_insert)
{
	node_t *temporary = *head;

	if(temporary != NULL)
	{
		while(temporary->next != NULL)
			temporary=temporary->next;

		node_to_insert->prev = temporary;
		temporary->next = node_to_insert;
		temporary=node_to_insert;
    } 
	else 
		*head = node_to_insert;
	
	return node_to_insert;
}

										// AUXILIARY FUNCTIONS //

void cin_nodes(node_t **head)
{
	node_t *tmp;
	int id;
	char name[10];
	int birth_year;
	int n;

	printf("\n    Dati numarul de noduri pe care doriti sa le introduceti: "); scanf("%d", &n);
	printf("\n");

	for(int i=0; i<n; i++)
	{
		tmp = load_node(&*head);
		printf("\n");
		insert_to_end(head, tmp);
	}
}

void create_head_even(node_t *head, node_t **head_even)
{
	node_t *tmp = NULL;

	if(head != NULL)
	{
		while(head != NULL)
		{
			if(head->id % 2 == 0)
			{
				tmp = create_node(head->id);
				insert_to_end(&*head_even, tmp);
			}
			head = head->next;
		}
	}
	else
		printf("Lista este vida");

}


void create_head_between(node_t *head, node_t **head_between)
{
	node_t *tmp = NULL;
	int min_val, max_val;

	if(head != NULL)
	{
		printf("\n    Dati valoarea minima: ");
		scanf("%d", &min_val);

		printf("\n    Dati valoarea maxima: ");
		scanf("%d", &max_val);

		while(head != NULL)
		{
			if((head->id >= min_val) && (head->id <= max_val))			
			{
				tmp = create_node(head->id);
				insert_to_end(&*head_between, tmp);
			}
			head=head->next;
		}
	}
	else
		printf("Lista este vida");
}

										// FUNCTIA DE AFISARE A LISTEI // 

void afisare_lista(node_t *head)
{
	node_t *temporary = head;

	if(temporary == NULL)
		printf("          Lista este goala");

	while(temporary != NULL)
	{
		printf("%d - ", temporary->id);
		temporary = temporary->next;
	}

	printf("\n");
}




#include <stdlib.h>
#include <stdio.h>


struct nod{

	int value;
	struct nod *next;

};

typedef struct nod node_t;


node_t *create_node(int value)
{
	node_t *new1 = malloc(sizeof(node_t));
	new1->value = value;
	new1->next = NULL;

	return new1;
}

node_t *insert_to_head(node_t **head, node_t *node_to_insert)
{
	node_to_insert->next=*head;
	*head=node_to_insert;

	return node_to_insert;
}


void afisare_lista(node_t *head)
{
	node_t *temporary = head;

	while(temporary != NULL)
	{
		printf("%d - ", temporary->value);
		temporary = temporary->next;

	}
	printf("\n");
}


int main()
{
	node_t *head=NULL;
	node_t *tmp;

	for(int i; i<25; i++)
	{
		tmp = create_node(i);
		insert_to_head(&head, tmp );
	}

	afisare_lista(head);




return 0;
}


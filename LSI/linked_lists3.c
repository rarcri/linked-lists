#include <stdlib.h>
#include <stdio.h>


struct nod{

	int value;
	struct nod *next;

};

typedef struct nod node_t;


node_t *create_node(int value)
{
	node_t *new1 = malloc(sizeof(node_t));
	new1->value = value;
	new1->next = NULL;

	return new1;
}

node_t *insert_to_head(node_t **head, node_t *node_to_insert)
{
	node_to_insert->next=*head;
	*head=node_to_insert;

	return node_to_insert;
}

node_t *insert_after_head(node_t **head, node_t *node_to_insert)
{
	node_t *temporary = *head;


	if(temporary != NULL)
	{
	while(temporary->next != NULL)
		temporary=temporary->next;
		
		temporary->next = node_to_insert;
		node_to_insert->next = NULL;
		temporary=node_to_insert;

    } else {
		*head = node_to_insert;
	
	}
	
	return node_to_insert;

}

node_t *insert_to_choice(node_t **head, int key, node_t *node_to_insert, int choice)
{
	node_t *temporary = *head;
	node_t *penultim = NULL;



	if(temporary != NULL)
	{
		while(temporary->value != key)
		{
			if(temporary->next != NULL)
			{

			penultim = temporary;
			temporary = temporary->next;
			
			} else printf("Cheia nu s-a gasit!");
			
		}


						// DACĂ ULTIMUL ELEMENT//
					

		if(temporary->next == NULL)
		{
			
			if(choice == 0)
				temporary->next=node_to_insert;
			else if(choice == 1)
			{	
				penultim->next=node_to_insert;
				node_to_insert=temporary;
			}
		}
		else{
			if(choice == 0)
			{	
				node_to_insert->next = temporary->next;
				temporary->next=node_to_insert;
			}	
			else if(choice == 1)
			{
				penultim->next=node_to_insert;
				node_to_insert->next=temporary;
				
			}
		}
				
	} else 
		return node_to_insert;	
}






void afisare_lista(node_t *head)
{
	while(head != NULL)
	{
		printf("%d - ", head->value);
		head = head->next;

	}
	printf("\n");
}


int main()
{
	node_t *head=NULL;
	node_t *tmp;

	for(int i; i<25; i++)
	{
		tmp = create_node(i);
		insert_after_head(&head, tmp );
	}

	afisare_lista(head);


			

	tmp = create_node(25);
	insert_to_choice(&head, 25, tmp, 1);

	afisare_lista(head);

return 0;
}


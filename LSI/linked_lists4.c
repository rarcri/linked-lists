#include <stdlib.h>
#include <stdio.h>


struct nod
{
	int value;
	struct nod *next;

} Node1;

typedef struct nod node_t;
				
											// FUNCTIONS //

node_t *create_node(int value)
{
	node_t *new1 = malloc(sizeof(node_t));
	new1->value = value;
	new1->next = NULL;

	return new1;
}


node_t *insert_to_head(node_t **head, node_t *node_to_insert)
{
	node_to_insert->next=*head;
	*head=node_to_insert;

	return node_to_insert;
}


node_t *insert_after_head(node_t **head, node_t *node_to_insert)
{
	node_t *temporary = *head;

	if(temporary != NULL)
	{
		while(temporary->next != NULL)
			temporary=temporary->next;

		temporary->next = node_to_insert;
		node_to_insert->next = NULL;
		temporary=node_to_insert;
    } 
	else 
		*head = node_to_insert;
	
	
	return node_to_insert;

}


node_t *insert_to_choice(node_t **head, int key, node_t *node_to_insert, int choice)
{
	node_t *temporary = *head;
	node_t *penultim = NULL;
	
	if(temporary != NULL)
	{
		while(temporary->value != key)
		{
			if(temporary->next != NULL)
			{
				penultim = temporary;
				temporary = temporary->next;
			}
			else
				printf("Cheia nu s-a gasit!");
		}

				// dacă am ajuns la ultimul //
					
		if(temporary->next == NULL)
		{
			if(choice == 0)
				temporary->next=node_to_insert;
			else if(choice == 1)
			{	
				penultim->next=node_to_insert;
				node_to_insert=temporary;
			}
		}
		else
		{
			if(choice == 0)
			{	
				node_to_insert->next = temporary->next;
				temporary->next=node_to_insert;
			}	
			else if(choice == 1)
			{
				penultim->next=node_to_insert;
				node_to_insert->next=temporary;
			}
		}
				
	} 
	else 

		return node_to_insert;	
}

													// CITIRE NOD //
													
void cin_node(node_t **head)
{
	node_t *tmp;
	int value;
	int n;
	printf("Dati numarul de noduri: "); scanf("%d", &n);

	for(int i=0; i<n; i++)
	{
		printf("Dati cheia nodului: ");
		scanf("%d", &value);
		tmp = create_node(value);
		insert_after_head(head, tmp);
	}

}


void cin_new_node(node_t **head)
{
	node_t *tmp = NULL;
	int n_value, value, choice;

	printf("Dati valoarea noului nod: ");
	scanf("%d", &n_value);

	printf("Dati valoarea față de care vreți să introduceți nodul: ");
	scanf("%d", &value);

	printf("Inserare inainte/dupa(1/0) de nodul cu valoarea %d: ", value);
	scanf("%d", &choice);
	
	tmp = create_node(n_value);
	insert_to_choice(&*head, value, tmp, choice);
	
}


void afisare_lista(node_t *head)
{
	node_t *temporary = head;

	while(temporary != NULL)
	{
		printf("%d - ", temporary->value);
		temporary = temporary->next;
	}

	printf("\n");
}

int main()
{
	node_t *head=NULL;

	cin_node(&head);
	afisare_lista(head);
	
	cin_new_node(&head);
	afisare_lista(head);

return 0;
}


#include <stdlib.h>
#include <stdio.h>


struct nod
{
	int value;
	struct nod *next;

} Node1;

typedef struct nod node_t;
				


node_t *create_node(int value);
node_t *insert_before_head(node_t **head, node_t *node_to_insert);
node_t *insert_after_head(node_t **head, node_t *node_to_insert);
node_t *find(node_t *head, int value);

void insert(node_t **head, int key, node_t *node_to_insert, int choice);
void delete_first(node_t **head);
void delete_last(node_t *head);
void delete();
void delete_all();

void cin_node(node_t **head);
void cin_new_node(node_t **head);
void afisare_lista(node_t *head);


int main()
{
	node_t *head=NULL;

	cin_node(&head);
	afisare_lista(head);
	
	cin_new_node(&head);
	afisare_lista(head);

return 0;
}

											// FUNCTIONS //

node_t *create_node(int value)
{
	node_t *new1 = malloc(sizeof(node_t));
	new1->value = value;
	new1->next = NULL;

	return new1;
}


node_t *insert_before_head(node_t **head, node_t *node_to_insert)
{
	node_to_insert->next=*head;
	*head=node_to_insert;

	return node_to_insert;
}


node_t *insert_after_head(node_t **head, node_t *node_to_insert)
{
	node_t *temporary = *head;

	if(temporary != NULL)
	{
		while(temporary->next != NULL)
			temporary=temporary->next;

		temporary->next = node_to_insert;
		node_to_insert->next = NULL;
		temporary=node_to_insert;
    } 
	else 
		*head = node_to_insert;
	
	
	return node_to_insert;

}


node_t *find(node_t *t_head, int value)
{	
	node_t *null=NULL;
	node_t *prev;

	if(t_head != NULL)
	{
		while(t_head->value != value)
		{
			if(t_head->next != NULL)
				t_head = t_head->next;
			else
			{	
				printf("Cheia nu s-a gasit");
				return null;
			}
		}
		
		return t_head;

	}
	else
		printf("Lista este goala.");


}


void insert(node_t **head, int key, node_t *node_to_insert, int choice)
{
	node_t *temporary = *head;
	node_t *penultim = NULL;
	
	if(temporary != NULL)
	{   
		if(temporary->value == key)
		{
			if(choice == 0)
			{
				node_to_insert->next = temporary;
				*head = node_to_insert;
			}
			else if(choice == 1)
			{
				node_to_insert->next = (*head)->next;
				(*head)->next = node_to_insert;
			}
			return;
		}

		while(temporary->value != key)
		{
			if(temporary->next != NULL)
			{
				penultim = temporary;
				temporary = temporary->next;
			}
			else
			{
				printf("Cheia nu s-a gasit!");
				return;
			}
		}

					
		if(temporary->next == NULL)
		{
			if(choice == 1)
				temporary->next=node_to_insert;
			else if(choice == 0)
			{	
				penultim->next=node_to_insert;
				node_to_insert=temporary;
			}
		}
		else
		{
			if(choice == 1)
			{	
				node_to_insert->next = temporary->next;
				temporary->next=node_to_insert;
			}	
			else if(choice == 0)
			{
				penultim->next=node_to_insert;
				node_to_insert->next=temporary;
			}
		}
				
	} 
	else 
		printf("Lista este goala");
}


void delete_first(node_t **head)
{
	node_t *tmp = *head;
	*head = (*head)->next;
	
	free(tmp);
}


void delete_last(node_t *head)
{	
	node_t *prev=NULL;
	
	if(head != NULL)
	{
		while(head->next != NULL)
		{
			prev = head;
			head = head->next;
		}
		prev->next = NULL;
		free(head);
	}
	else
		printf("\nLista este goala.");
}


void delete(node_t *head, int value_to_find)
{
	node_t *prev, *tmp;
	tmp = head;

	if(tmp != NULL)
	{
		while(tmp->value != value_to_find)
		{
			if(tmp->next != NULL)
			{
				prev = tmp;
				tmp = tmp->next;
			}
			else
			{
				printf("Cheia nu a fost gasita");
				return;
			}
		}
		
		prev->next = tmp->next;
		free(tmp);
		printf("\nNodul cu cheia %d a fost sters", value_to_find);
	}
	else 
		printf("\nLista este goala.");
}

void delete_all(node_t **head)
{
	while(*head != NULL)
		delete_first(&*head);
}

void cin_node(node_t **head)
{
	node_t *tmp;
	int value;
	int n;
	printf("Dati numarul de noduri: "); scanf("%d", &n);

	for(int i=0; i<n; i++)
	{
		printf("Dati cheia nodului %d: ", i+1);
		scanf("%d", &value);
		tmp = create_node(value);
		insert_after_head(head, tmp);
	}

}


void cin_new_node(node_t **head)
{
	node_t *tmp = NULL;
	int n_value, value, choice;

	printf("Dati valoarea noului nod: ");
	scanf("%d", &n_value);

	printf("Dati valoarea față de care vreți să introduceți nodul: ");
	scanf("%d", &value);

	printf("Inserare inainte/dupa(0/1) de nodul cu valoarea %d: ", value);
	scanf("%d", &choice);
	
	tmp = create_node(n_value);
	insert(&*head, value, tmp, choice);
	
}


void afisare_lista(node_t *head)
{
	node_t *temporary = head;

	while(temporary != NULL)
	{
		printf("%d - ", temporary->value);
		temporary = temporary->next;
	}

	printf("\n");
}

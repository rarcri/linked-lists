#include <stdlib.h>
#include <stdio.h> 

										// Struct declaration //
struct nod
{
	int value;
	struct nod *next;

} Node1;

		// I defined a new type of nod
typedef struct nod node_t;
				
		// These are the functions that return pointer to node_t 
		// FUNCTIONS (lines 57-269) 
node_t *create_node(int value);
node_t *insert_before_head(node_t **head, node_t *node_to_insert);
node_t *insert_to_end(node_t **head, node_t *node_to_insert);
node_t *find(node_t *head, int value);

		// These functions return nothing
		// FUNCTIONS 
void insert(node_t **head, int key, node_t *node_to_insert, int choice);
void insert_to_begining(node_t **head, node_t *node_to_insert);
void delete_first(node_t **head);
void delete_last(node_t *head);
void delete(node_t **head, int value_to_find);
void delete_all(node_t **head);

		// These functions use the FUNCTIONS 
		// AUXILIARY FUNCTIONS (lines 273-379) 
void cin_node(node_t **head); 
void cin_new_node(node_t **head);
void cin_new_node_to_beginning(node_t **head);
void cin_new_node_to_end(node_t **head);
void cin_new_node_before(node_t **head);
void cin_new_node_after(node_t **head);
void cin_delete(node_t **head);

		// Functia de afisare a unei liste
void afisare_lista(node_t *head);

		// MENU FUNCTIONS
int menu(node_t *head);
void main_menu(node_t **head);


int main()
{
	node_t *head=NULL;
	main_menu(&head); // line 426
	return 0;
}

											// FUNCTIONS //

node_t *create_node(int value)
{
	node_t *new1 = malloc(sizeof(node_t));
	new1->value = value;
	new1->next = NULL;

	return new1;
}


node_t *insert_before_head(node_t **head, node_t *node_to_insert)
{
	node_to_insert->next=*head;
	*head=node_to_insert;

	return node_to_insert;
}



node_t *insert_to_end(node_t **head, node_t *node_to_insert)
{
	node_t *temporary = *head;

	if(temporary != NULL)
	{
		while(temporary->next != NULL)
			temporary=temporary->next;

		temporary->next = node_to_insert;
		node_to_insert->next = NULL;
		temporary=node_to_insert;
    } 
	else 
		*head = node_to_insert;
	
	return node_to_insert;
}


node_t *find(node_t *t_head, int value)
{	
	node_t *null=NULL;
	node_t *prev;

	if(t_head != NULL)
	{
		while(t_head->value != value)
		{
			if(t_head->next != NULL)
				t_head = t_head->next;
			else
			{	
				printf("	Cheia nu s-a gasit");
				return null;
			}
		}
		
		return t_head;

	}
	else
		printf("	Lista este goala.");


}


void insert(node_t **head, int key, node_t *node_to_insert, int choice)
{
	node_t *temporary = *head;
	node_t *penultim = NULL;
	
	if(temporary != NULL)
	{   
		if(temporary->value == key)
		{
			if(choice == 0)
			{
				node_to_insert->next = temporary;
				*head = node_to_insert;
			}
			else if(choice == 1)
			{
				node_to_insert->next = (*head)->next;
				(*head)->next = node_to_insert;
			}
			return;
		}

		while(temporary->value != key)
		{
			if(temporary->next != NULL)
			{
				penultim = temporary;
				temporary = temporary->next;
			}
			else
			{
				printf("	Cheia nu s-a gasit!");
				return;
			}
		}

					
		if(temporary->next == NULL)
		{
			if(choice == 1)
				temporary->next=node_to_insert;
			else if(choice == 0)
			{	
				penultim->next=node_to_insert;
				node_to_insert=temporary;
			}
		}
		else
		{
			if(choice == 1)
			{	
				node_to_insert->next = temporary->next;
				temporary->next=node_to_insert;
			}	
			else if(choice == 0)
			{
				penultim->next=node_to_insert;
				node_to_insert->next=temporary;
			}
		}
				
	} 
	else 
		printf("	Lista este goala");
}


void delete_first(node_t **head)
{
	node_t *tmp = *head;
	*head = (*head)->next;
	
	free(tmp);
}


void delete_last(node_t *head)
{	
	node_t *prev=NULL;
	
	if(head != NULL)
	{
		while(head->next != NULL)
		{
			prev = head;
			head = head->next;
		}
		prev->next = NULL;
		free(head);
	}
	else
		printf("\n    Lista este goala.");
}


void delete(node_t **head, int value_to_find)
{
	node_t *prev, *tmp;
	tmp = *head;

	if(tmp != NULL)
	{
		if(tmp->value == value_to_find)
		{
			*head = (*head)->next;
			free(tmp);
		}

		while(tmp->value != value_to_find)
		{
			if(tmp->next != NULL)
			{
				prev = tmp;
				tmp = tmp->next;
			}
			else
			{
				printf("	Cheia nu a fost gasita\n");
				return;
			}
		}
	
		if(tmp->next != NULL)
		{
			prev->next = tmp->next;
			free(tmp);
		}
		else
		{
			prev->next = NULL;
			free(tmp);
		}
		printf("\n    Nodul cu cheia %d a fost sters\n", value_to_find);
	}
	else 
		printf("\nLista este goala.");
}


void delete_all(node_t **head)
{
	while((*head)->next != NULL)
		delete_first(&*head);
	delete_first(&*head);
}

										// AUXILIARY FUNCTIONS //

void cin_node(node_t **head)
{
	node_t *tmp;
	int value;
	int n;
	printf("\n    Dati numarul de noduri: "); scanf("%d", &n);
	printf("\n");

	for(int i=0; i<n; i++)
	{
		printf("    Dati cheia nodului %d: ", i+1);
		scanf("%d", &value);
		tmp = create_node(value);
		insert_to_end(head, tmp);
	}
}


void cin_new_node(node_t **head)
{
	node_t *tmp = NULL;
	int n_value, value, choice;

	printf("    Dati valoarea noului nod: ");
	scanf("%d", &n_value);

	printf("    Dati valoarea față de care vreți să introduceți nodul: ");
	scanf("%d", &value);

	printf("    Inserare inainte/dupa(0/1) de nodul cu valoarea %d: ", value);
	scanf("%d", &choice);
	
	tmp = create_node(n_value);
	insert(&*head, value, tmp, choice);
}


void cin_new_node_to_beginning(node_t **head)
{
	node_t *tmp; 
	int n_value;
	
	printf("    Dati valoarea noului nod: ");
	scanf("%d", &n_value);

	tmp = create_node(n_value);
	insert_before_head(&*head, tmp);
}


void cin_new_node_to_end(node_t **head)
{
	node_t *tmp;
	int n_value;

	printf("    Dati valoare noului nod: ");
	scanf("%d", &n_value);

	tmp = create_node(n_value);

	insert_to_end(&*head, tmp);
}


void cin_new_node_before(node_t **head)
{
	node_t *tmp = NULL;
	int n_value, value;

	printf("    Dati valoarea noului nod: ");
	scanf("%d", &n_value);

	printf("    Dati valoarea inainte de care vreți să introduceți nodul: ");
	scanf("%d", &value);

	tmp = create_node(n_value);
	insert(&*head, value, tmp, 0);
	
}


void cin_new_node_after(node_t **head)
{
	node_t *tmp = NULL;
	int n_value, value;

	printf("    Dati valoarea noului nod: ");
	scanf("%d", &n_value);

	printf("    Dati valoarea dupa care vreți să introduceți nodul: ");
	scanf("%d", &value);

	tmp = create_node(n_value);
	insert(&*head, value, tmp, 1);
	
}


void cin_delete(node_t **head)
{
	int key;

	printf("\n    Care este cheia nodului pe care doriti sa il stergeti: ");
	scanf("%d", &key);

	delete(&*head, key);	
}

										// FUNCTIA DE AFISARE A LISTEI // 

void afisare_lista(node_t *head)
{
	node_t *temporary = head;

	if(temporary == NULL)
		printf("          Lista este goala");

	printf("          ");
	while(temporary != NULL)
	{
		printf("%d - ", temporary->value);
		temporary = temporary->next;
	}

	printf("\n");
}

int menu(node_t *head)
{
	int choice;

	system("cls");
	printf("\n	          _____________________MENIU__________________\n\
	          | Citeste o lista de la tastatura     - 1  |\n\
                  | Insereaza un nod la inceput         - 2  |\n\
	          | Insereaza un nod la final           - 3  |\n\
	          | Insereaza un nod inaintea unei chei - 4  |\n\
	          | Insereaza un nod dupa o cheie       - 5  |\n\
	          | Sterge primul nod                   - 6  |\n\
	          | Sterge ultimul nod                  - 7  |\n\
	          | Sterge un nod dupa cheie            - 8  |\n\
	          | Sterge toata lista                  - 9  |\n\
	          | Terminare                           - 10 |\n\
	          |__________________________________________|\n\n");
	printf("    Lista actuala: ");
	afisare_lista(head);
	printf("\n    Alegeti o optiune: ");
	scanf("%d", &choice);

	return choice;
}

void main_menu(node_t **head)
{
	int choice=1;

	while(choice < 10)
	{
		choice = menu(*head);

		switch(choice)
		{
			case 1:
				cin_node(&*head);
				break;
			case 2:
				cin_new_node_to_beginning(&*head);
				break;
			case 3: 
				cin_new_node_to_end(&*head);
				break;
			case 4:
				cin_new_node_before(&*head);
				break;
			case 5:
				cin_new_node_after(&*head);
				break;
			case 6:
				delete_first(&*head);	
				break;
			case 7:
				delete_last(*head);
				break;
			case 8:
				cin_delete(&*head);
				break;
			case 9:
				delete_all(&*head);
				break;
			
			default:
		    printf("    Programul s-a terminat\n\n");
		}
	}
}


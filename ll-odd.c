#include <stdio.h>
#include <stdlib.h>
#include <string.h>


struct node{
	int value;
	struct node* next;
};

typedef struct node node_t;






node_t *creaza_nod_nou(int value)
{
	node_t *result = malloc(sizeof(node_t));
	result->value = value;
	result->next = NULL;
	return result;

}


node_t *insereaza_head(node_t **head, node_t *node_to_insert)
{
	node_to_insert->next = *head;
	*head = node_to_insert;
	return node_to_insert;

}

void printlist(node_t *head)
{

	node_t *temporary = head;

	while(temporary != NULL)
	{
		if(temporary->value % 2 == 1)
			printf("%d - ", temporary->value);
		temporary = temporary->next;
	}
	printf("\n");
	
}


int main(){

	node_t *head=NULL;
	node_t *tmp;

	int n;
	printf("Dati nr de noduri de introdus: "); scanf("%d", &n);


		int value;

 		for(int i=0; i<n; i++)
		{
			printf("Dati valoarea nodului %d: ", i);
			scanf("%d", &value);
			tmp=creaza_nod_nou(value);
			insereaza_head(&head, tmp);
		
		}



	printf("\n Elementele impare sunt -> ");
	printlist(head);




	return 0;

}




